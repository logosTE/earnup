angular.module('ads', ['ionic', 'openfb', 'ngResource','ui.utils','ngCordova'])

.run(function ($rootScope, $state, $ionicPlatform, $window, OpenFB,$location,$ionicLoading) {
      $rootScope.$on('loading:show', function() {
        $ionicLoading.show({template: 'Loading...',duration:60*1000,delay:1000,hideOnStateChange:true});
      });
      $rootScope.$on('loading:hide', function() {
        $ionicLoading.hide()
      });
      $rootScope.globalServerUrl = "http://lodge39.com";
      // $rootScope.globalServerUrl="http://77.121.81.147";
      $rootScope.logged = localStorage.getItem("logged");
      OpenFB.init('803761662981098');
      $ionicPlatform.ready(function () {
        ////
        var ad_units = {
          ios : {
            banner: 'ca-app-pub-8972676458491669/4744131633' // or DFP format "/6253334/dfp_example_ad"
          },
          android : {
            banner: 'ca-app-pub-8972676458491669/4744131633'
          }
        };
        var admobid = ( /(android)/i.test(navigator.userAgent) ) ? ad_units.android : ad_units.ios;
        if(AdMob) AdMob.createBanner( admobid.banner );
        /////
        ionic.Platform.isFullScreen = true;
        if (window.StatusBar) {
          StatusBar.styleDefault();
        }
        screen.lockOrientation('portrait');
        document.addEventListener("backbutton", onBackKeyDown, false);
        document.addEventListener("deviceready", initPushwoosh, true);
      });

      $rootScope.$on('$stateChangeStart', function (event, toState) {
        if (toState.name !== "signin" && toState.name !== "forgotpassword" && toState.name !== "registration" && localStorage.getItem("token") === null) {
          $state.go('signin');
        }
      });
      function onBackKeyDown() {
        var cur = $state.current.name;
        switch (cur) {
          case "homeselected":$state.go('home');break;
          case "selectedvideo":$state.go('homeselected');break;
          case "settings":$state.go('new');break;
          case "tabs.profile":$state.go('new');break;
          case "facebook":$state.go('home');break;
          case "home":$state.go('new');break;
          case "forgotpassword":$state.go('signin'); break;
          case "registration":$state.go('signin');break;
          case "changepassword":$state.go('home');break;
          case "new":localStorage.clear();$state.go('signin');break;
          case "random-list":$state.go('myoffers');break;
          case "finance":$state.go('settings');break;
          case "card":$state.go('settings');break;
          case "withdraw":$state.go('settings');break;
          case "mmedia":$state.go('myoffers');break;
          case "signin":
            if (navigator.app) {navigator.app.exitApp();}
            else if (navigator.device) {navigator.device.exitApp();}break;
        }
      };
      $rootScope.logout = function () {
        localStorage.clear();
        $state.go('signin');
        if (navigator.app) {
          navigator.app.exitApp();
        } else if (navigator.device) {
          navigator.device.exitApp();
        }
      };
      $rootScope.faqShow = function(){
        $state.go('faq');
      };
      $rootScope.panel=function(){
        $state.go('panel');
      }

      function initPushwoosh() {
        if (device.platform == "Android") {
        var pushNotification = window.plugins.pushNotification;
        //set push notifications handler
        document.addEventListener('push-notification', function (event) {
          var title = event.notification.title;
          var userData = event.notification.userdata;
          if (typeof(userData) != "undefined") {
            console.warn('user data: ' + JSON.stringify(userData));
          }
        });
        //initialize Pushwoosh with projectid: "GOOGLE_PROJECT_ID", appid : "PUSHWOOSH_APP_ID". This will trigger all pending push notifications on start.
        pushNotification.onDeviceReady({ projectid: "227847839726", appid: "BF936-7B630" });
        pushNotification.registerDevice(
            function (status) {
              var pushToken = status;
              console.warn('push token: ' + pushToken);
            },
            function (status) {
              console.warn(JSON.stringify(['failed to register ', status]));
            }
        );
      }
        else{
          var pushNotification = window.plugins.pushNotification;
          document.addEventListener('push-notification', function(event) {
            //get the notification payload
            var notification = event.notification;
            alert(notification.aps.alert);
            pushNotification.setApplicationIconBadgeNumber(0);
          });
          pushNotification.onDeviceReady({pw_appid:"BF936-7B630"});
          pushNotification.registerDevice(
              function(status) {
                var deviceToken = status['deviceToken'];
                console.warn('registerDevice: ' + deviceToken);
              },
              function(status) {
                console.warn('failed to register : ' + JSON.stringify(status));
                alert(JSON.stringify(['failed to register ', status]));
              }
          );
          pushNotification.setApplicationIconBadgeNumber(0);
        }
    }
    })


.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
      $httpProvider.interceptors.push(function($rootScope) {
        return {
          request: function(config) {
            $rootScope.$broadcast('loading:show')
            return config
          },
          response: function(response) {
            $rootScope.$broadcast('loading:hide')
            return response
          }
        }
      })
    $stateProvider
        .state('signin', {url: "/sign-in", templateUrl: "sign-in.html", controller: 'SignInCtrl'})
        .state('forgotpassword', {url: "/forgot-password", templateUrl: "forgot-password.html", controller: "ForgotCtrl"})
        .state('registration', {url: "/registration", templateUrl: "registration.html", controller: "RegistrationCtrl"})
        .state('homeselected', {url: "/home-selected", templateUrl: "home-selected.html", controller: "HomeSelectedCtrl"})
        .state('finance', {url: "/finance", templateUrl: "finance.html", controller: "FinanceCtrl"})
        .state('faq', {url: "/faq", templateUrl: "faq.html", controller: "FaqCtrl"})
        .state('ref',{url:'/ref',templateUrl:'ref.html',controller:"RefCtrl"})
        .state('selectedvideo', {url: "/selected-video", templateUrl: "selected-video.html", controller: "SelectedVideoCtrl"})
        .state('card', {url: "/card", templateUrl: "card.html",controller:"CardCtrl"})
        .state('change-password', {url: "/change-password", templateUrl: "change-password.html",controller:"ChangePasswordCtrl"})
        .state('withdraw', {url: "/withdraw", templateUrl: "withdraw.html",controller:"WithdrawCtrl"})
        .state('new',{url: "/new",templateUrl:"new.html",controller:"NewPageCtrl"})
        .state('mmedia',{url: "/mmedia-page",templateUrl:"mmedia-page.html",controller:"MMediaCtrl"})
        .state('home', {url: "/home",templateUrl: "home.html", controller: 'HomeTabCtrl'})
        .state('profile', {url: "/profile",templateUrl: "profile.html",controller: "ProfileTabCtrl"})
        .state('invite',{url:"/invite",templateUrl:"invite.html",controller:"InviteCtrl"})
        .state('myoffers',{url:"/my-offers",templateUrl:"my-offers.html",controller:"MyOffersCtrl"})
        .state('presents',{url:"/presents", templateUrl:"presents.html",controller:"PresentsCtrl"})
        .state('redeem',{url:"/redeem", templateUrl:"redeem.html",controller:"RedeemCtrl"})
        .state('facebook', {url: "/facebook",templateUrl: "facebook.html", controller: "FacebookTabCtrl"})
        .state('settings', {url: "/settings",templateUrl: "settings.html",controller: "SettingCtrl"})
        .state('panel', {url: "/panel",templateUrl: "panel.html",controller: "PanelCtrl"})
        .state('random-list', {url: "/random-list",templateUrl: "random-list.html",controller: "RandomListCtrl"});
        $urlRouterProvider.otherwise("/sign-in");
})

.controller('SignInCtrl', function($scope, $state, OpenFB,$http, $rootScope, BalanceService) {
      function convertImgToBase64(url, callback, outputFormat){
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var img = new Image;
        img.crossOrigin = 'Anonymous';
        img.onload = function(){
          canvas.height = img.height;
          canvas.width = img.width;
          ctx.drawImage(img,0,0);
          var dataURL = canvas.toDataURL(outputFormat || 'image/jpeg');
          callback.call(this, dataURL);
          // Clean up
          canvas = null;
        };
        img.src = url;
      }
      $scope.user={};
      $scope.word=/^[a-zA-Z0-9]+$/; //password regexp
      $scope.emailPattern=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      $scope.text=" ";
      var url=$rootScope.globalServerUrl+'/profile/login/';
      var leftButton=document.querySelector(".buttons.left-buttons");
      var rightButton=document.querySelector(".buttons.right-buttons");
      leftButton.style.visibility="hidden";
      rightButton.style.visibility="hidden";
      $scope.signIn = function() {
        OpenFB.login('email,read_stream,publish_stream')
          .then(
            function () {
              $state.go('home');
            },
            function () {
             window.plugins.toast.show('Fblogin error', 'short', 'center');
            });
      };

      $scope.submit=function(){
        var data=JSON.stringify($scope.user);
        $http({
            url: url,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            data: 'data='+data
          }).success(function(data) {
            if(data.login===true){
              console.log(data);
              localStorage.setItem("email", data.email);
              localStorage.setItem("username", data.username);// work with it
              localStorage.setItem("token", data.token);  // work with it
              localStorage.setItem("password",$scope.user.password);
              localStorage.setItem("id", data.id);
              localStorage.setItem("limit",data.limit);
              BalanceService.setBalance(data.balance);  // work with it
              localStorage.setItem("logged","true");
              var imgUrl=$rootScope.globalServerUrl+'/public/uploads/'+data.avatar;
              if(!localStorage.getItem("photo")){
                convertImgToBase64(imgUrl, function(base64Img){
                  localStorage.setItem("photo",base64Img);
                });
              }

              $state.go('new');
              leftButton.style.visibility="";
              rightButton.style.visibility="";
            }
            else{
             window.plugins.toast.show('Incorrect login or password', 'long', 'bottom');
             // $state.go('signin');
            }
          }).error(function(){
          window.plugins.toast.show('Connection problems', 'long', 'bottom');
        });
        };
      if(localStorage.getItem("logged")==="true"){
        $scope.user.password=localStorage.getItem("password");
        $scope.user.login=localStorage.getItem("email");
        $scope.submit();
        leftButton.style.visibility="";
        rightButton.style.visibility="";
      }
})

.controller('CardCtrl',function($scope,$http,$rootScope,$ionicNavBarDelegate,$state){
      var url=$rootScope.globalServerUrl+'/withdraw/getPaymentMethod/';
      var setUrl=$rootScope.globalServerUrl+'/withdraw/setPaymentMethod';
      var updUrl=$rootScope.globalServerUrl+'/withdraw/updatePaymentMethod';
      $scope.ch=true;
      var data={
        email : localStorage.getItem("email"),
        token : localStorage.getItem("token")
      }
      $http({
        url: url,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
        data: "data="+JSON.stringify(data)
      }).success(function(data){
        console.log(data.method);
        if(!data.method){
          $scope.payments=data.methodList;
          $scope.ch=false;
          console.log($scope.payments);
        }
        else{
          $scope.descr=data.methodDescription[0].description;
          $scope.payments=data.methodList;
          $scope.type=findById(data.methodList,data.methodDescription[0].type);
          $scope.ch=true;

        }
      }).error(function(data){window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
       });
      function findById(source, id) {
        for (var i = 0; i < source.length; i++) {
          if (source[i].id === id) {
            return source[i];
          }
        }
        throw "Couldn't find object with id: " + id;
      }
      $scope.update=function(descr,type){
        data.type=type.id;
        data.description=descr;
        $http({
          url: updUrl,
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
          data: "data="+JSON.stringify(data)
        }).success(function(){
          window.plugins.toast.show('Updated', 'short', 'center');
        }).error(function(data){
          window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
        });
      };

      $scope.send=function(descr,type){
        data.type=type.id;
        data.description=descr;


        $http({
          url: setUrl,
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
          data: "data="+JSON.stringify(data)
        }).success(function(data){
          window.plugins.toast.show('Updated', 'short', 'center');
        }).error(function(data){
          window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
        });

      }
      $scope.back=function(){
        $ionicNavBarDelegate.back();
      }
    })

.controller('WithdrawCtrl',function($scope,$http,BalanceService,$rootScope,$ionicNavBarDelegate,$state,$timeout){
      var url=$rootScope.globalServerUrl+'/withdraw/getPaymentMethod/';
      var witUrl=$rootScope.globalServerUrl+'/withdraw/request/';
      var lastWithUrl=$rootScope.globalServerUrl+'/withdraw/getLastWithdraw/';
$scope.with={};
      $scope.block=false;
      $scope.balance=BalanceService.getBalance();
      $scope.lastPayments=[];
      var withdrawData={};
      var x;
      var dataAuth={
        email : localStorage.getItem("email"),
        token : localStorage.getItem("token")
      };

      $http({
        url: url,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
        data: "data="+JSON.stringify(dataAuth)
      }).success(function(data){
        if(!data.method){
           $scope.notification='No payment method found';
          $scope.block=true;
        }
        else{
          $scope.block=false;
         withdrawData={
            descr:data.methodDescription[0].description,
            method:findById(data.methodList,data.methodDescription[0].type),
            email : localStorage.getItem("email"),
            token : localStorage.getItem("token")
          };
          x=findById(data.methodList,data.methodDescription[0].type);
          getLast(x);
        }
      }).error(function(data){
        window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
      });

      $scope.withdraw=function(){
        if(($scope.with.amount>0)&&($scope.with.amount<=$scope.balance)){
          $scope.block=true;
          withdrawData.amount=$scope.with.amount;
          $timeout(function(){
            $scope.block=false;
          },5000);
          $http({
            url: witUrl,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            data: "data="+JSON.stringify(withdrawData)
          }).success(function(data){
            $scope.notification='Request sended to administrator';
            getLast(x)
            BalanceService.setBalance(data.balance);
            $scope.balance=BalanceService.getBalance();
            window.plugins.toast.show('Request sended', 'short', 'center');
          }).error(function(data){
            window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
          });
        }
        else{
          $scope.notification='Wrong amount';
        }

      };
      function getLast(param){
        $scope.lastPayments=[];
        $http({
          url: lastWithUrl,
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
          data: "data="+JSON.stringify(dataAuth)
        }).success(function(last){

          angular.forEach(last.splice(0,3),function(item){
            $scope.lastPayments.push({
              time:new Date(item.time*1000).toISOString().slice(0, 10),
              amount:item.amount,
              type: param.type,
              status:item.status==0?"waiting":"paid"});
          });

        }).error(function(data){
          window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
        });
      }
      function findById(source, id) {
        for (var i = 0; i < source.length; i++) {
          if (source[i].id === id) {
            return source[i];
          }
        }
        throw "Couldn't find object with id: " + id;
      }
      $scope.back=function(){
        $ionicNavBarDelegate.back();
      }
    })

.controller('NewPageCtrl',function($scope,$state,BalanceService,$rootScope,$http,$timeout){
      $scope.balance=BalanceService.getBalance();
      $timeout(function(){
        $rootScope.photoRestored=localStorage.getItem("photo")||'img/anonym.png';
      },500);
      $scope.curId='0';
      $scope.curId=localStorage.getItem('id');
      $scope.refCount='0';
      var data={};
      data.token=localStorage.getItem("token");
      data.email=localStorage.getItem("email");
      var url=$rootScope.globalServerUrl+'/profile/getReferrals';
      $http({
        url: url,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
        data: "data="+JSON.stringify(data)
      }).success(function(res){
        console.log(res);
        $scope.refCount=res.length;

      }).error(function(data){
        window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
      });
      $scope.viewAds=function(){
        $state.go('home');
      };
      $scope.viewSettings=function(){
        $state.go('settings');
      };
      $scope.viewRef=function(){
        $state.go('ref');
      };
      $scope.viewProfile=function(){
        $state.go('profile');
      };
      $scope.viewOffers=function(){
        $state.go('myoffers');
      };
      $scope.viewMMedia=function(){
        $state.go('mmedia');
      };
      $scope.viewRedeem=function(){
        $state.go('redeem');
      }
      $scope.viewInvite=function(){
        $state.go('invite');
      }
      $scope.viewPresents=function(){
        $state.go('presents');
      }
    })

.controller('MMediaCtrl',function($scope){
      var mMedia_units = {
        ios : {banner : "177365",interstitial : "177364"},
        android : { banner : "177367", interstitial : "177369"}
      };
      var mMedia_adid = ( /(android)/i.test(navigator.userAgent) ) ? mMedia_units.android : mMedia_units.ios;
        if(mMedia){
          mMedia.prepareInterstitial( {adId:mMedia_adid.interstitial, autoShow:true} );
        }
    })

.controller('FaqCtrl',function($scope){
      $scope.faqText="Here should be a lot of text";
    })

.controller('PanelCtrl',function($scope,$state){
      $scope.viewProfile=function(){
        $state.go('profile');
      };
      $scope.viewInvited=function(){
        $state.go('ref');
      }
      //$scope.faqText="Here should be a lot of text";
    })

.controller('FinanceCtrl',function($scope,BalanceService,$rootScope,$http,$ionicNavBarDelegate,$state){

      var urlMonth=$rootScope.globalServerUrl+'/withdraw/getMonthAmount';
      var urlAnnual=$rootScope.globalServerUrl+'/withdraw/getAnnualAmount';
      var urlTotal=$rootScope.globalServerUrl+'/withdraw/getTotalAmount';

      $scope.balance=BalanceService.getBalance();
      $scope.curr_ammount=$scope.balance;
      var data={
        email : localStorage.getItem("email"),
        token : localStorage.getItem("token")
      };
      $http({
        url: urlMonth,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
        data: "data="+JSON.stringify(data)
      }).success(function(data){
        $scope.monthly_amount=data.monthAmount;
      }).error(function(data){
        window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
      });
      $http({
        url: urlAnnual,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
        data: "data="+JSON.stringify(data)
      }).success(function(data){
        $scope.annual_amount=data.annualAmount;
      }).error(function(data){
        window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
      });
      $http({
        url: urlTotal,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
        data: "data="+JSON.stringify(data)
      }).success(function(data){
        $scope.till_amount=data.totalAmount;
      }).error(function(data){
        window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
      });


      $scope.text='Earnings by time';
      $scope.back=function(){
        $ionicNavBarDelegate.back();
      }
    })

.controller('ForgotCtrl',function($scope, $http,$state, $rootScope){
     $scope.request=function(email){
       var url=$rootScope.globalServerUrl+'/profile/resetPassword/';
       var data=email;
       $http({
         url: url,
         method: "POST",
         headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
         data: 'data='+JSON.stringify(data)
       }).success(function(data){
         if(data.resetPassword==="false"){
           window.plugins.toast.show('Sorry, your email not founded in database', 'long', 'bottom');
         }
         else{
           window.plugins.toast.show('Your new password sended to your email', 'long', 'bottom');
           $state.go('signin');
         }
       }).error(function(data){
         window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
       });
     };

    })

.controller('SettingCtrl',function($scope,$state){
      $scope.clear=function(){
        localStorage.clear();
        $state.go('signin');
      };
      $scope.viewCard=function(){
        $state.go('card');
      }
      $scope.viewFinance=function(){
        $state.go('finance');
      }
      $scope.viewWithdraw=function(){
        $state.go('withdraw');
      };
    })

.controller('RefCtrl',function($scope,$http,$rootScope,$state){
      $scope.source='';
      var data={};
      data.token=localStorage.getItem("token");
      data.email=localStorage.getItem("email");
      var url=$rootScope.globalServerUrl+'/profile/getReferrals';
      $http({
        url: url,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
        data: "data="+JSON.stringify(data)
      }).success(function(res){
        console.log(res);
        $scope.source=res;

      }).error(function(data){
        window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
      });

    })

.controller('RandomListCtrl',function($scope,$ionicModal,$timeout,$http,$rootScope,BalanceService,$ionicGesture){
      var url=$rootScope.globalServerUrl+'/main/getAdLoop/';
      var hitUrl=$rootScope.globalServerUrl+'/main/loopHit/';
      var imageUrl=$rootScope.globalServerUrl+"/public/picture/";
      var adLoopList=[];
      var array=[];
      var currentBlock;
      var index=0;
      var button=angular.element(document.querySelector("#next-button"));
      $scope.balance=BalanceService.getBalance();

      $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        backdropClickToClose: false
      }).then(function(modal) {
        $scope.modal = modal;
        $http({
          url: url,
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
          data: {}
        }).success(function(data){
          adLoopList=data
          next(adLoopList);
        }).error(function(error){
          console.log(error);
        })
      });

      $scope.clicked=function(){
        index+=4;
        $scope.modal.show();
        next(adLoopList);
      };

      function next(data){
        button.addClass("hide");
        currentBlock=data.slice(index,index+4);
        if(currentBlock.length>=4){
          $scope.source=imageUrl+currentBlock[0].src;
          $timeout(function(){
            console.log('1');
            $scope.source=imageUrl+currentBlock[1].src;
            $timeout(function(){
              console.log('2');
              $scope.source=imageUrl+currentBlock[2].src;
              $timeout(function(){
                console.log('3');
                $scope.source=imageUrl+currentBlock[3].src;
                $timeout(function(){
                  angular.forEach(currentBlock,function(item){
                    array.push(parseInt(item.id));
                  });
                },currentBlock[3].duration*1000).then(function(){
                  if($scope.modal.isShown()){
                    console.log('watched');
                    $scope.closeModal();
                    button.removeClass("hide");
                    sendWatchedAd(array);
                  }
                });
              },currentBlock[2].duration*1000);
            },currentBlock[1].duration*1000);
          },currentBlock[0].duration*1000);
        }
      }

      function sendWatchedAd(array){
        var data={};
        data.ad = array;
        data.email = localStorage.getItem("email");
        data.token = localStorage.getItem("token");

        $http({
          url: hitUrl,
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
          data: "data=" + JSON.stringify(data)
        }).success(function (data) {
          console.log(data);
          BalanceService.setBalance(data.balance);
          $scope.balance=BalanceService.getBalance();
          array=[];
        });
      }
      $scope.closeModal = function() {
        $scope.modal.hide();
      };
      //Cleanup the modal when we're done with it!
      $scope.$on('$destroy', function() {
        $scope.modal.remove();
      });
      $timeout(function(){
        $scope.modal.show();
      },500);

    })

.controller('HomeTabCtrl', function($scope, $http, HomeService, $state,$resource, $rootScope,$timeout, BalanceService,$sce) {
    $scope.category={};
    $scope.balance=BalanceService.getBalance();
    var url = $rootScope.globalServerUrl;
    var data={};
    data.id=localStorage.getItem("id");

      ///timezone
      function getTimezoneName() {
        tmSummer = new Date(Date.UTC(2005, 6, 30, 0, 0, 0, 0));
        so = -1 * tmSummer.getTimezoneOffset();
        tmWinter = new Date(Date.UTC(2005, 12, 30, 0, 0, 0, 0));
        wo = -1 * tmWinter.getTimezoneOffset();

        if (-660 == so && -660 == wo) return 'Pacific/Midway';
        if (-600 == so && -600 == wo) return 'Pacific/Tahiti';
        if (-570 == so && -570 == wo) return 'Pacific/Marquesas';
        if (-540 == so && -600 == wo) return 'America/Adak';
        if (-540 == so && -540 == wo) return 'Pacific/Gambier';
        if (-480 == so && -540 == wo) return 'US/Alaska';
        if (-480 == so && -480 == wo) return 'Pacific/Pitcairn';
        if (-420 == so && -480 == wo) return 'US/Pacific';
        if (-420 == so && -420 == wo) return 'US/Arizona';
        if (-360 == so && -420 == wo) return 'US/Mountain';
        if (-360 == so && -360 == wo) return 'America/Guatemala';
        if (-360 == so && -300 == wo) return 'Pacific/Easter';
        if (-300 == so && -360 == wo) return 'US/Central';
        if (-300 == so && -300 == wo) return 'America/Bogota';
        if (-240 == so && -300 == wo) return 'US/Eastern';
        if (-240 == so && -240 == wo) return 'America/Caracas';
        if (-240 == so && -180 == wo) return 'America/Santiago';
        if (-180 == so && -240 == wo) return 'Canada/Atlantic';
        if (-180 == so && -180 == wo) return 'America/Montevideo';
        if (-180 == so && -120 == wo) return 'America/Sao_Paulo';
        if (-150 == so && -210 == wo) return 'America/St_Johns';
        if (-120 == so && -180 == wo) return 'America/Godthab';
        if (-120 == so && -120 == wo) return 'America/Noronha';
        if (-60 == so && -60 == wo) return 'Atlantic/Cape_Verde';
        if (0 == so && -60 == wo) return 'Atlantic/Azores';
        if (0 == so && 0 == wo) return 'Africa/Casablanca';
        if (60 == so && 0 == wo) return 'Europe/London';
        if (60 == so && 60 == wo) return 'Africa/Algiers';
        if (60 == so && 120 == wo) return 'Africa/Windhoek';
        if (120 == so && 60 == wo) return 'Europe/Amsterdam';
        if (120 == so && 120 == wo) return 'Africa/Harare';
        if (180 == so && 120 == wo) return 'Europe/Athens';
        if (180 == so && 180 == wo) return 'Africa/Nairobi';
        if (240 == so && 180 == wo) return 'Europe/Moscow';
        if (240 == so && 240 == wo) return 'Asia/Dubai';
        if (270 == so && 210 == wo) return 'Asia/Tehran';
        if (270 == so && 270 == wo) return 'Asia/Kabul';
        if (300 == so && 240 == wo) return 'Asia/Baku';
        if (300 == so && 300 == wo) return 'Asia/Karachi';
        if (330 == so && 330 == wo) return 'Asia/Calcutta';
        if (345 == so && 345 == wo) return 'Asia/Katmandu';
        if (360 == so && 300 == wo) return 'Asia/Yekaterinburg';
        if (360 == so && 360 == wo) return 'Asia/Colombo';
        if (390 == so && 390 == wo) return 'Asia/Rangoon';
        if (420 == so && 360 == wo) return 'Asia/Almaty';
        if (420 == so && 420 == wo) return 'Asia/Bangkok';
        if (480 == so && 420 == wo) return 'Asia/Krasnoyarsk';
        if (480 == so && 480 == wo) return 'Australia/Perth';
        if (540 == so && 480 == wo) return 'Asia/Irkutsk';
        if (540 == so && 540 == wo) return 'Asia/Tokyo';
        if (570 == so && 570 == wo) return 'Australia/Darwin';
        if (570 == so && 630 == wo) return 'Australia/Adelaide';
        if (600 == so && 540 == wo) return 'Asia/Yakutsk';
        if (600 == so && 600 == wo) return 'Australia/Brisbane';
        if (600 == so && 660 == wo) return 'Australia/Sydney';
        if (630 == so && 660 == wo) return 'Australia/Lord_Howe';
        if (660 == so && 600 == wo) return 'Asia/Vladivostok';
        if (660 == so && 660 == wo) return 'Pacific/Guadalcanal';
        if (690 == so && 690 == wo) return 'Pacific/Norfolk';
        if (720 == so && 660 == wo) return 'Asia/Magadan';
        if (720 == so && 720 == wo) return 'Pacific/Fiji';
        if (720 == so && 780 == wo) return 'Pacific/Auckland';
        if (765 == so && 825 == wo) return 'Pacific/Chatham';
        if (780 == so && 780 == wo) return 'Pacific/Enderbury'
        if (840 == so && 840 == wo) return 'Pacific/Kiritimati';
        return 'US/Pacific';
      }
      ////timezone


       $http({
          url: url+"/main/getAdCategories",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
          data: "data="+JSON.stringify(data)
        }).success(function(data){
          $scope.category=data;
        }).error(function(data){
         window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
       });

        $scope.getCategoryVideos=function(id){
          var mas=localStorage.getItem("watched_today_category") || '';
          if(mas!==null && mas.indexOf(id.default_ad)>-1){
            console.log(mas);
          }else {
            localStorage.setItem('lastCat', id.category);  //workaround
            var videoThumbUrl = url + "/public/video/thumb/";
            var videoUrl = url + "/public/video/";
            var imageThumbUrl = url + "/public/picture/thumb/";
            var imageUrl = url + "/public/picture/";
            var idSet = {
              id: localStorage.getItem("id"),
              category: localStorage.getItem('lastCat'),
              timezone: getTimezoneName()
            };

            if (device.platform === "Android" && id.src) {
              $timeout(function(){screen.lockOrientation('landscape');},1000);
              VideoPlayer.play(videoUrl + id.src, '',
                  function (msg) {
                    var seconds = msg / 1000;
                    if (seconds > id.duration) {

                      data.ad = id.default_ad;
                      data.answer = 1;
                      data.email = localStorage.getItem("email");
                      data.token = localStorage.getItem("token");
                      $http({
                        url: url + "/main/autoHit",
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                        data: "data=" + JSON.stringify(data)
                      }).success(function (data) {
                        if (data.hit === true) {
                          BalanceService.setBalance(data.balance);
                        }
                      }).error(function (data) {
                        window.plugins.toast.show('Connection problems', 'long', 'bottom');
                        $state.go('signin');
                      });
                    }
                    screen.lockOrientation('portrait');
                  },
                  function (err) {
                    console.log(err);
                  });
            } else if (id.src) {
              $scope.source = $sce.trustAsResourceUrl(videoUrl + id.src);
              $scope.video = true;
              var video = document.getElementById("vidHome");

              video.addEventListener('play', function () {
                console.log("play");
                video.play();
              }, false);

              video.addEventListener("pause", function () {
                console.log("pause");
                video.remove();
              }, false);

              video.addEventListener("ended", function () {
                console.log("ended");
                video.remove();
              }, false);
            }
/////////////// //add to watched
            mas=JSON.parse("[" + mas + "]");
            mas.push(id.default_ad);
            localStorage.setItem('watched_today_category',mas);
          }
          $http({
            url: url+"/main/getAdByCategory",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            data: "data="+JSON.stringify(idSet)
          }).success(function(data){
            for (index = 0; index < data.ad.length; ++index) {
              if(data.ad[index].type==="video"){
                data.ad[index].thumbnail= videoThumbUrl+data.ad[index].thumbnail;
                data.ad[index].src= videoUrl+data.ad[index].src;
                if(("watched" in data)) {
                  for (var i = 0; i < data.watched.length; i++) {
                    if (data.ad[index].id === data.watched[i].id) {
                      data.ad[index].watched = true;
                    }
                  }
                }
              }
              else{
                data.ad[index].thumbnail= imageThumbUrl+data.ad[index].thumbnail;
                data.ad[index].src= imageUrl+data.ad[index].src;
                if(("watched" in data)) {
                  for (var i = 0; i < data.watched.length; i++) {
                    if (data.ad[index].id === data.watched[i].id) {
                      data.ad[index].watched = true;
                    }
                  }
                }
              }
            }
            localStorage.setItem("currentLimit",data.limit);
            HomeService.setVideoUrl(data.ad); //result[0]
            HomeService.setVideosByCategory(data.ad);
          }).error(function(data){
            window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
          });
           $state.go('homeselected');
       }

   })

.controller('ProfileTabCtrl', function($scope, $http, ProfileService,BalanceService, ajaxServices, $q,$state,$timeout, $rootScope, $filter) {
    var data={}; $scope.ref={};
    var url= $rootScope.globalServerUrl;
      var urlGetRef=url+'/profile/getReferrerName';
    $scope.user={};

    data.token=localStorage.getItem("token");
    data.email=localStorage.getItem("email");
      $scope.CountryList = $http({method:"GET",url: url+'/tag/getCountries'}) ;
      $scope.EducationList = $http({method:"GET",url:url+'/tag/getEducations'});
      $scope.SexList = $http({method:"GET",url:url+'/tag/getSex'});
      $scope.ProfessionList = $http({method:"GET",url:url+'/tag/getProfessions'});
      $scope.userData =$http({
          url: url + "/profile/getProfile",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
          data: "data=" + JSON.stringify(data),
          timeout:60*1000
        }).success(function (data) {
          console.log(data);
          $scope.user.date = data[0].birthday ? $filter('date')(data[0].birthday * 1000, "yyyy-MM-dd") : "";
          $scope.user.firstName = data[0].firstName;
          $scope.user.lastName = data[0].lastName;
          $scope.user.street = data[0].street;
          $scope.user.referrer = data[0].referrer;
          $scope.user.email = data.email;
          $scope.user.zip = parseInt(data[0].zip);
          $scope.user.phone = data[0].phone;
        }).error(function (data) {
          window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('new');
          console.log(data);
          console.log("error");
        });

      $scope.takePic = function() {
        var options =   {
          quality: 50,
          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: 2,     // 0:Photo Library, 1=Camera, 2=Saved Photo Album
          encodingType: 0     // 0=JPG 1=PNG
        };
        navigator.camera.getPicture(onSuccess,onFail,options);
      };
      var onSuccess = function(FILE_URI) {
        console.log(FILE_URI);
        $scope.send(FILE_URI);
        document.getElementById("myImg").src="img/waiting.gif";
        $scope.$apply();
      };
      var onFail = function(e) {
        console.log("On fail " + e);
      };
      function onUploadSuccess(res){
        console.log(res);
        var obj=res.response;
        if(JSON.parse(obj).error){
          document.getElementById("myImg").src='img/anonym.png';
          window.plugins.toast.show('You should try to upload *.jpg<800kb', 'long', 'center');
        }else{
          var imgUrl=url+'/public/uploads/'+JSON.parse(obj).uploadPicture;
          document.getElementById("myImg").src=imgUrl;
          convertImgToBase64(imgUrl, function(base64Img){
            localStorage.setItem("photo",base64Img);
          });
        }

      }
      function onUploadFail(fa){
        console.log(fa);

      }
      $scope.send = function(url) {
        var options = new FileUploadOptions();
        var params={};
        params.data=JSON.stringify({email:localStorage.getItem("email"),token:localStorage.getItem("token")});
        options.fileKey="userfile";
        options.params=params;
        var ft = new FileTransfer();
        console.log(options);
        ft.upload(url, encodeURI("http://lodge39.com/profile/uploadImage"), onUploadSuccess, onUploadFail, options);
      };

      function convertImgToBase64(url, callback, outputFormat){
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var img = new Image;
        img.crossOrigin = 'Anonymous';
        img.onload = function(){
          canvas.height = img.height;
          canvas.width = img.width;
          ctx.drawImage(img,0,0);
          var dataURL = canvas.toDataURL(outputFormat || 'image/jpeg');
          callback.call(this, dataURL);
          // Clean up
          canvas = null;
        };
        img.src = url;
      }

      $q.all([$scope.CountryList, $scope.EducationList, $scope.SexList, $scope.userData,$scope.ProfessionList]).then(function(res) {
        $scope.CountryList=res[0].data;
        $scope.EducationList=res[1].data;
        $scope.SexList=res[2].data;
        $scope.userData=res[3].data;
        $scope.ProfessionList=res[4].data;
        $scope.CityList=ajaxServices.getProjects($scope.userData[0].country).success(
            function (data) {
             $scope.CityList=data;
             $scope.user.city=$scope.CityList[findById($scope.CityList,$scope.userData[0].city)].id;
            });


      $scope.user.country=$scope.CountryList[$scope.userData[0].country-1];
      $scope.user.education=$scope.EducationList[$scope.userData[0].education-1];
      $scope.user.sex=$scope.SexList[$scope.userData[0].sex];
      $scope.user.profession=$scope.ProfessionList[$scope.userData[0].profession-1];

      $http({
        url: urlGetRef,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
        data: "data="+JSON.stringify($scope.user.referrer)
      }).success(function(res){
        $scope.refName=res.first_name+' '+res.last_name;
      }).error(function(data){
        window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('new');
      });


      function findById(source, id) {
        for (var i = 0; i < source.length; i++) {
          if (source[i].id === id) {
            return i;//source[i];
          }
        }
        throw "HERE Couldn't find object with id: " + id;
      }
    },function(error){console.log(error);});

      $scope.changed=function(ev){ //if user entered old phone
        if($scope.user.phone===$scope.userData[0].phone){
          ev.myForm.phone.$setValidity('unique',true);
        }
      };

      $scope.setCountry=function(id){
        $http({method:"GET",url:url+'/tag/getCities/'+id,timeout:60*1000})   //ready
            .success(function(data){
              $scope.CityList=data;
            }).error(function(data){
              window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('new');
            });
      };


    $scope.submit=function(){
      var profileData={
        "birthday" : $scope.user.date?Math.round(+new Date($scope.user.date)/1000):"",
        "id" : localStorage.getItem('id'),
        "country" : $scope.user.country?$scope.user.country.id:"",
        "education" : $scope.user.education?$scope.user.education.id:"",
        "profession" : $scope.user.profession?$scope.user.profession.id:"",
        "sex" : $scope.user.sex?$scope.user.sex.id:"",
        "email" : data.email,
        "token" : data.token,
        "firstName" : $scope.user.firstName?$scope.user.firstName:"",
        "lastName" : $scope.user.lastName?$scope.user.lastName:"",
        "phone" : $scope.user.phone?$scope.user.phone:null,
        "ref" : $scope.user.referrer?$scope.user.referrer:null,
        "street" : $scope.user.street?$scope.user.street:"",
        "city" : $scope.user.city?$scope.user.city:"",
        "zip" : $scope.user.zip?parseInt($scope.user.zip):null
      };

      console.log(profileData);
      $http({
          url: url+"/profile/setProfile/",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
          data: "data="+encodeURIComponent(JSON.stringify(profileData))
      }).success(function(){
         window.plugins.toast.show('Profile updated', 'short', 'center');
      }).error(function(){
       window.plugins.toast.show('Error', 'short', 'center');
      });
    };
      $scope.checkRef=function(){
        var urlRef=url+'/profile/checkReferrer/';
        var urlGetRef=url+'/profile/getReferrerName/';

        if($scope.user.referrer.length!=0)
        {
          $http({
            url: urlRef,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            data: "data="+JSON.stringify($scope.user.referrer)
          }).success(function(data){
            if(data=="false"){
              $scope.refName='Not found';
            }
            else {
              $http({
                url: urlGetRef,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: "data="+JSON.stringify($scope.user.referrer)
              }).success(function(res){
                if(res.first_name!==null){$scope.refName=res.first_name;}
                else{$scope.refName='Anonymous user';}
                if(res.last_name!==null){$scope.refName=$scope.refName+' '+res.last_name;}
              }).error(function(data){
                window.plugins.toast.show('Connection problems', 'long', 'bottom');
                $state.go('signin');
              });
            }
          }).error(function(data){
            window.plugins.toast.show('Connection problems', 'long', 'bottom');
            $state.go('signin');
          });
        }
        else{
          $scope.refName='';
        }
      };

})

.controller('FacebookTabCtrl', function($scope, $http, BalanceService) {
             $scope.balance=BalanceService.getBalance();
            var ref = window.open('http://facebook.com', '_blank', 'location=yes');
            //ref.addEventListener('loadstart', function() { alert(event.url); });
            })

.controller('InviteCtrl', function($scope, $http,$rootScope) {
      $scope.userInfo={
        message:"",
        who:""
      };
      var url= $rootScope.globalServerUrl;
    $scope.username=localStorage.getItem("username");

      $scope.mail=function(){
        var inviteObj={
          username:$scope.username,
          token:localStorage.getItem("token"),
          email:localStorage.getItem("email"),
          emails:$scope.userInfo.who,
          message:$scope.userInfo.message
        };
        $http({
          url: url+"/invite/email",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
          data: "data="+JSON.stringify(inviteObj)
        }).success(function(data){
          if(data.length<1){
            window.plugins.toast.show('Sent','short','center');
          }else{window.plugins.toast.show('Check email','short','center');}
        }).error(function(data){
          //window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
        });
      }
    })

    .controller('MyOffersCtrl', function($scope, $http,$state) {
      $scope.viewMmedia=function(){
        var mMedia_units = {
          ios : {banner : "177365",interstitial : "177364"},
          android : { banner : "177367", interstitial : "177369"}
        };
        var mMedia_adid = ( /(android)/i.test(navigator.userAgent) ) ? mMedia_units.android : mMedia_units.ios;
        if(mMedia){
          mMedia.prepareInterstitial( {adId:mMedia_adid.interstitial, autoShow:true} );
        }
        console.log(mMedia);
      }
      $scope.viewAdmob=function(){

      }
      $scope.viewRandom=function(){
        $state.go('random-list');
      }
    })

.controller('RedeemCtrl', function($scope, $http,$state,$rootScope,BalanceService) {
      var url= $rootScope.globalServerUrl;
      var witUrl=url+'/withdraw/requestReward';
      var withGetIntention=url+'/withdraw/getIntention';
      $scope.serv=url+"/public/reward/";
      var data={
        email : localStorage.getItem("email"),
        token : localStorage.getItem("token")};
      $scope.intention='';
      $http({
        url: withGetIntention,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
        data: "data="+JSON.stringify(data)
      }).success(function(res){
        if(res.name==undefined){
          res={id: "1",
            image: "cake.png",
            name: "Cake",
            price: "100",
            reward: "1",
            user: "310"}
        }
        $scope.intention=res;
      }).error(function(res){
        window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
      });

      $scope.redeem=function(){
        if(parseInt(BalanceService.getBalance())>=$scope.intention.price){
          $scope.block=true;
          var  withdrawData={
            rewardId:$scope.intention.reward,
            name:$scope.intention.name,
            price:$scope.intention.price,
            email : localStorage.getItem("email"),
            token : localStorage.getItem("token")
          };
          $http({
            url: witUrl,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            data: "data="+JSON.stringify(withdrawData)
          }).success(function(res){
            console.log(res);
            BalanceService.setBalance(res.balance);
            $scope.block=false;
            window.plugins.toast.show('Sent','short','center');
          }).error(function(res){
            window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
          });
        }
        else{
          window.plugins.toast.show('Not enough points', 'long', 'center');
        }
      };

      $scope.choose=function(){
        $state.go('presents');
      }
    })

    .controller('PresentsCtrl', function($scope, $http,$rootScope,$state) {
      var url= $rootScope.globalServerUrl;
      var intentionUrl=url+'/withdraw/setIntention';
      $scope.serv=url+"/public/reward/";
      $scope.source='';
      $http.get(url+"/withdraw/reward").success(function(res){
        $scope.source=res;
      });

      $scope.redeem=function(item){
        var  withdrawData={
          rewardId:item.id,
          name:item.name,
          price:item.price,
          email : localStorage.getItem("email"),
          token : localStorage.getItem("token")
        };
          $http({
            url: intentionUrl,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            data: "data="+JSON.stringify(withdrawData)
          }).success(function(data){
            console.log(data);
            $state.go('redeem');
          }).error(function(data){
            //window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
          });


      }
    })

.controller('RegistrationCtrl', function($scope, $http,$location,IdService, $rootScope,$state,$timeout) {
      var serverUrl=$rootScope.globalServerUrl;
      $scope.user={}; $scope.ref={};
      $scope.wordPattern=/^[a-zA-Z0-9]+$/; //password regexp
      $scope.namePattern=/^[a-zA-Z]+$/;
      $scope.emailPattern=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      $scope.checkRef=function(){
        var url=serverUrl+'/profile/checkReferrer/';
        var urlGetRef=serverUrl+'/profile/getReferrerName/';

        if($scope.user.referrer.length!=0)
        {
          $http({
            url: url,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            data: "data="+JSON.stringify($scope.user.referrer)
          }).success(function(data){
            if(data=="false"){
              $scope.refName='Not found';
            }
            else {
              $http({
                url: urlGetRef,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: "data="+JSON.stringify($scope.user.referrer)
              }).success(function(res){
                if(res.first_name!==null){$scope.refName=res.first_name;}
                else{$scope.refName='Anonymous user';}
                if(res.last_name!==null){$scope.refName=$scope.refName+' '+res.last_name;}
              }).error(function(data){
                window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
              });
            }
          }).error(function(data){
            window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
          });
        }
        else{
          $scope.refName='';
        }
      };

      $scope.submit=function(){

        var data={
          lastName:$scope.user.lastName,
          firstName:$scope.user.firstName,
          username:$scope.user.username,
          email:$scope.user.email,
          password:$scope.user.password,
          referrer:($scope.user.referrer)?$scope.user.referrer:null,
          phone:$scope.user.phone
        };

          var url=serverUrl+'/profile/registrationFirstStep/';

            $http({
              url: url,
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: "data=" + JSON.stringify(data)
            }).success(function(data){
              if(data.registration){
                window.plugins.toast.show('Success! Now you can login', 'long', 'center');
                $state.go('signin');
              }
              else{
                window.plugins.toast.show('Connection problems', 'long', 'bottom');
                $state.go('signin');
                console.log(data);
              }
            }).error(function(data){
              window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
            });
      };
                
   })


.controller('HomeSelectedCtrl', function($scope,$ionicSlideBoxDelegate ,HomeService, BalanceService, $http, $state,$timeout,$rootScope){
      var url = $rootScope.globalServerUrl;
      //VideoPlayer.play("http://techslides.com/demos/sample-videos/small.mp4");
      ///timezone
      function getTimezoneName() {
        tmSummer = new Date(Date.UTC(2005, 6, 30, 0, 0, 0, 0));
        so = -1 * tmSummer.getTimezoneOffset();
        tmWinter = new Date(Date.UTC(2005, 12, 30, 0, 0, 0, 0));
        wo = -1 * tmWinter.getTimezoneOffset();

        if (-660 == so && -660 == wo) return 'Pacific/Midway';
        if (-600 == so && -600 == wo) return 'Pacific/Tahiti';
        if (-570 == so && -570 == wo) return 'Pacific/Marquesas';
        if (-540 == so && -600 == wo) return 'America/Adak';
        if (-540 == so && -540 == wo) return 'Pacific/Gambier';
        if (-480 == so && -540 == wo) return 'US/Alaska';
        if (-480 == so && -480 == wo) return 'Pacific/Pitcairn';
        if (-420 == so && -480 == wo) return 'US/Pacific';
        if (-420 == so && -420 == wo) return 'US/Arizona';
        if (-360 == so && -420 == wo) return 'US/Mountain';
        if (-360 == so && -360 == wo) return 'America/Guatemala';
        if (-360 == so && -300 == wo) return 'Pacific/Easter';
        if (-300 == so && -360 == wo) return 'US/Central';
        if (-300 == so && -300 == wo) return 'America/Bogota';
        if (-240 == so && -300 == wo) return 'US/Eastern';
        if (-240 == so && -240 == wo) return 'America/Caracas';
        if (-240 == so && -180 == wo) return 'America/Santiago';
        if (-180 == so && -240 == wo) return 'Canada/Atlantic';
        if (-180 == so && -180 == wo) return 'America/Montevideo';
        if (-180 == so && -120 == wo) return 'America/Sao_Paulo';
        if (-150 == so && -210 == wo) return 'America/St_Johns';
        if (-120 == so && -180 == wo) return 'America/Godthab';
        if (-120 == so && -120 == wo) return 'America/Noronha';
        if (-60 == so && -60 == wo) return 'Atlantic/Cape_Verde';
        if (0 == so && -60 == wo) return 'Atlantic/Azores';
        if (0 == so && 0 == wo) return 'Africa/Casablanca';
        if (60 == so && 0 == wo) return 'Europe/London';
        if (60 == so && 60 == wo) return 'Africa/Algiers';
        if (60 == so && 120 == wo) return 'Africa/Windhoek';
        if (120 == so && 60 == wo) return 'Europe/Amsterdam';
        if (120 == so && 120 == wo) return 'Africa/Harare';
        if (180 == so && 120 == wo) return 'Europe/Athens';
        if (180 == so && 180 == wo) return 'Africa/Nairobi';
        if (240 == so && 180 == wo) return 'Europe/Moscow';
        if (240 == so && 240 == wo) return 'Asia/Dubai';
        if (270 == so && 210 == wo) return 'Asia/Tehran';
        if (270 == so && 270 == wo) return 'Asia/Kabul';
        if (300 == so && 240 == wo) return 'Asia/Baku';
        if (300 == so && 300 == wo) return 'Asia/Karachi';
        if (330 == so && 330 == wo) return 'Asia/Calcutta';
        if (345 == so && 345 == wo) return 'Asia/Katmandu';
        if (360 == so && 300 == wo) return 'Asia/Yekaterinburg';
        if (360 == so && 360 == wo) return 'Asia/Colombo';
        if (390 == so && 390 == wo) return 'Asia/Rangoon';
        if (420 == so && 360 == wo) return 'Asia/Almaty';
        if (420 == so && 420 == wo) return 'Asia/Bangkok';
        if (480 == so && 420 == wo) return 'Asia/Krasnoyarsk';
        if (480 == so && 480 == wo) return 'Australia/Perth';
        if (540 == so && 480 == wo) return 'Asia/Irkutsk';
        if (540 == so && 540 == wo) return 'Asia/Tokyo';
        if (570 == so && 570 == wo) return 'Australia/Darwin';
        if (570 == so && 630 == wo) return 'Australia/Adelaide';
        if (600 == so && 540 == wo) return 'Asia/Yakutsk';
        if (600 == so && 600 == wo) return 'Australia/Brisbane';
        if (600 == so && 660 == wo) return 'Australia/Sydney';
        if (630 == so && 660 == wo) return 'Australia/Lord_Howe';
        if (660 == so && 600 == wo) return 'Asia/Vladivostok';
        if (660 == so && 660 == wo) return 'Pacific/Guadalcanal';
        if (690 == so && 690 == wo) return 'Pacific/Norfolk';
        if (720 == so && 660 == wo) return 'Asia/Magadan';
        if (720 == so && 720 == wo) return 'Pacific/Fiji';
        if (720 == so && 780 == wo) return 'Pacific/Auckland';
        if (765 == so && 825 == wo) return 'Pacific/Chatham';
        if (780 == so && 780 == wo) return 'Pacific/Enderbury'
        if (840 == so && 840 == wo) return 'Pacific/Kiritimati';
        return 'US/Pacific';
      }
      ////timezone
     function getFreshVideos() {
        var videoThumbUrl = url + "/public/video/thumb/";
        var videoUrl = url + "/public/video/";
        var imageThumbUrl = url + "/public/picture/thumb/";
        var imageUrl = url + "/public/picture/";
        var idSet = {
          id: localStorage.getItem("id"),
          category: localStorage.getItem('lastCat'),
          timezone: getTimezoneName()
        };

        $http({
          url: url + "/main/getAdByCategory",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
          data: "data=" + JSON.stringify(idSet)
        }).success(function (data) {
          for (index = 0; index < data.ad.length; ++index) {
            if (data.ad[index].type === "video") {
              data.ad[index].thumbnail = videoThumbUrl + data.ad[index].thumbnail;
              data.ad[index].src = videoUrl + data.ad[index].src;
              if (("watched" in data)) {
                for (var i = 0; i < data.watched.length; i++) {
                  if (data.ad[index].id === data.watched[i].id) {
                    data.ad[index].watched = true;
                  }
                }
              }
            }
            else {
              data.ad[index].thumbnail = imageThumbUrl + data.ad[index].thumbnail;
              data.ad[index].src = imageUrl + data.ad[index].src;
              if (("watched" in data)) {
                for (var i = 0; i < data.watched.length; i++) {
                  if (data.ad[index].id === data.watched[i].id) {
                    data.ad[index].watched = true;
                  }
                }
              }
            }
          }
          localStorage.setItem("currentLimit", data.limit);
          HomeService.setVideoUrl(data.ad); //result[0]
          HomeService.setVideosByCategory(data.ad);
        }).error(function(data){
          window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
        });
      }
      getFreshVideos();
    $scope.img=false;
    $scope.currentPage = 0;
    $scope.pageSize = 9;
    $scope.data = {};
    $scope.balance=BalanceService.getBalance();
    $timeout(function(){
      $scope.data=HomeService.getVideosList();
      $scope.img=true;
    },2000);


  $scope.numberOfPages=function(){
    return Math.ceil($scope.data.length/$scope.pageSize);
  };
  $scope.getTimes=function(){
    //return new Array(Math.ceil($scope.data.length/$scope.pageSize));
  };
  $scope.change=function(index){
    $scope.currentPage=index;
    setTimeout(function(){
      $ionicSlideBoxDelegate.update();
    },5000);
    console.log(index);
  };


  $scope.show=function(id){
    var cur=localStorage.getItem("currentLimit");
    var limit=localStorage.getItem("limit");

    if(parseInt(cur)<parseInt(limit)){
      if(!$scope.data[id].watched) {
        HomeService.setUrl(HomeService.getUrl(id));
        HomeService.setMediaType(id);
        HomeService.setMediaDuration(id);
        HomeService.setMediaId(id);
        $state.go('selectedvideo');
      }
      else{
        console.log('watched' + $scope.data[id].watched);
        window.plugins.toast.show('Watched today', 'short', 'center');
      }
    }
    else{
      console.log("limit expired");
      window.plugins.toast.show('Limit expired', 'short', 'center');
    }
  };

$scope.backBut=function(){
  $state.go('home');
}
})

.controller('ChangePasswordCtrl',function($scope,$http,$rootScope, $state){
      $scope.user={};
      $scope.wordPattern=/^[a-zA-Z0-9]+$/; //password regexp
      var email=localStorage.getItem("email");
      var url=$rootScope.globalServerUrl+'/profile/updatePassword/';



      $scope.submit=function(){
        $scope.data={
          login:localStorage.getItem("email"),
          password:$scope.user.oldPassword
        };
        console.log("login:"+$scope.data);
        $http({
          url: $rootScope.globalServerUrl+'/profile/login/',
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
          data: "data=" + JSON.stringify($scope.data)
        }).success(function(data){
          if(data.login===true){
            $scope.update={
              password:$scope.user.password,
              email:localStorage.getItem("email"),
              token:data.token
            };
            localStorage.setItem("token",data.token);
            console.log("update:"+$scope.update);
            $http({
              url: url,
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: "data=" + JSON.stringify($scope.update)
            }).success(function(data){
              window.plugins.toast.show('Updated', 'short', 'center');
              $state.go('home');
            }).error(function(data){
              window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
            });
          }
          else{
            window.plugins.toast.show('Wrong old password', 'short', 'center');
          }

        }).error(function(data){
          window.plugins.toast.show('Connection problems', 'long', 'bottom');$state.go('signin');
        });
      }
})

.controller('SelectedVideoCtrl', function($scope,HomeService,$ionicNavBarDelegate, BalanceService, $ionicPopup,$ionicModal, $ionicSlideBoxDelegate,$location, $state, $timeout ,$sce,$rootScope,$http) {

      $scope.counter = 0;
      $scope.status_source="img/smile-happy.jpeg";
      $scope.minPlayingTime = HomeService.getMediaDuration();
      $scope.countdown=$scope.minPlayingTime;
      var stopped,stopback;
      $scope.mediaId = HomeService.getMediaId();
      $scope.balance = BalanceService.getBalance();
      var readyState = false;
      var mytimeout;
      var data = {};
      var AdTextImage = document.getElementById("AdTextImage");
      var AdTextVideo = document.getElementById("AdTextVideo");
      $scope.onTimeout = function () {
        $scope.counter++;
        console.log($scope.counter);
        var mytimeout = $timeout($scope.onTimeout, 1000);
      };

      if (HomeService.getMediaType() === "picture") {
        $scope.viewName = "Picture";
        $scope.source = $sce.trustAsResourceUrl(HomeService.getU());
        var init = function () {
          $timeout(function () {
            $scope.openModal();
          }, 0);

          $timeout(function () {
            $scope.closeModal();
          }, $scope.minPlayingTime * 1000);
        };


      $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-left-right'
      }).then(function (modal) {
        $scope.timerImage();
        $scope.modal = modal;
      });

      $scope.openModal = function () {
        $scope.modal.show();
      };

      $scope.closeModal = function () {
        $timeout.cancel(stopped);
        $scope.countdown=$scope.countdown-1;
        console.log($scope.countdown);

        if($scope.countdown){
          $scope.notShown="true";
           stopback=$timeout(function() {
           $ionicNavBarDelegate.back();
           //  window.history.back();
           },3000);
        }
        else{
          console.log('should be false');
          asd();
          $timeout.cancel(stopback);
          $scope.notShown="false";
        }
        $scope.modal.hide();
      };
      $scope.$on('$destroy', function () {
        $scope.modal.remove();
        $scope.modal1.remove();
      });
      init();
    }

  else{
    if(device.platform==="Android"){
      $scope.video="false";
      $timeout(function(){screen.lockOrientation('landscape');},1000);

      VideoPlayer.play(HomeService.getU(),'',function(msg){
       var seconds = msg/1000;
        if(seconds<$scope.minPlayingTime){
          $scope.$apply(function(){$scope.notShown=true;});
        }
        else{
          console.log('more');
          asd();
        }
        screen.lockOrientation('portrait');
      },function(err){console.log(err);});
    }
    else {   //not andoid
      $scope.video = "true";
      $scope.viewName="Video";
      $scope.source = $sce.trustAsResourceUrl(HomeService.getU());
        var time=0;

       $timeout(function(){
         var video = document.getElementById("vid");
         video.addEventListener('timeupdate',function(event){
           time=video.currentTime;
         },false);

         video.addEventListener('play', function () {
           video.play();
           //mytimeout = $timeout($scope.onTimeout, 1000);
         }, false);

         video.addEventListener("pause", function () {
           console.log(time);
           if (time >= $scope.minPlayingTime) {

             time = 0;
             video.remove();
             $scope.video=false;
             $scope.notShown="false";
             asd();
           }
           else {
             $timeout(function() {
               $scope.notShown="true";
             },0);
             $timeout(function() {
               $ionicNavBarDelegate.back();
             },3000);
           }
         }, false);
       },0);

   }
 };
function asd(){
            var adUrl = $rootScope.globalServerUrl + "/main/getAdQuestion/";
            $http.get(adUrl + $scope.mediaId).then(function (response) {

              $scope.questions = response.data.question;
              $scope.answer = response.data.answers;
              $scope.click = function (id,index) {
                if(!$scope.voted) {
                  $scope.clicked = index;
                  data.ad = $scope.mediaId;
                  data.answer = id;
                  data.email = localStorage.getItem("email");
                  data.token = localStorage.getItem("token");

                  $http({
                    url: $rootScope.globalServerUrl + "/main/hit",
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    data: "data=" + JSON.stringify(data)
                  }).success(function (data) {
                    $scope.voted = true;
                    if (data.hit === true) {
                      BalanceService.setBalance(data.balance);
                      $scope.messageFromHitFirst = "Great";
                      $scope.messageFromHit = ", You answered right and got paid";

                    }
                    else {
                      $scope.messageFromHit = "You will answer better next time";
                    }
                    readyState = true;
                    $timeout(function () {
                      $ionicNavBarDelegate.back();
                    }, 3000);

                  }).error(function (data) {
                    window.plugins.toast.show('Connection problems', 'long', 'bottom');
                    $state.go('signin');
                  });
                }
              }
            });
};
      $ionicModal.fromTemplateUrl('image-status-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modal1 = modal;
      });
      $scope.timerImage = function() {
        stopped = $timeout(function() {

            $scope.countdown=$scope.countdown-1;
            console.log($scope.countdown);
            $scope.timerImage();
        }, 1000);
      };
      $scope.backBut=function(){
        $state.go('homeselected');
      }

})

.service('ProfileService', function($http){
  var profile={};
  return {
    getProfile: function(){
      return profile;
    },
    setProfile: function(data){
      profile=data;
    }
  };
})

.service('HomeService', function($rootScope, $http){
  var videosList=[];
  var videoUrl="";
  var serverUrl=$rootScope.globalServerUrl;
  var questions=[];
      var url;
      var type;
      var duration;
      var mediaId;

  return{
    setVideosByCategory: function(data){
      videosList=data;
    },
    getVideosList: function(){
      return videosList;
    },
    setVideoUrl: function(data){
      videoUrl=data;
    },
    getVideoUrl: function(){
      return videoUrl.src;
    },
    getMediaType: function(){
      return type;
    },
    setMediaType: function(id){
      type=videoUrl[id].type;
    },
    getMediaDuration: function(){
      return duration;
    },
    setMediaDuration: function(id){
      duration=videoUrl[id].duration;
    },
    getMediaId: function(){
      return mediaId;
    },
    setMediaId: function(id){
      mediaId=videoUrl[id].id;
    },
    getUrl:function(id){
      return videoUrl[id].src;
    },
    setUrl:function(src){
        url=src;
    },
    getU:function(){
      return url;
    }
  };
})

.service('BalanceService',function(){
      return {
        setBalance:function(balance){
          balance=parseFloat(balance).toFixed(2);
          console.log(balance);
          localStorage.setItem("balance",balance);
        },
        getBalance: function(){
          return localStorage.getItem("balance");
        }
      }
})

.service('IdService', function(){
    var id={};     
         return {
            getId:function(){
                return id;
            },
            setId:function(data){
                id=data;
            }
         };
})

.directive('validated', ['$parse', function($parse) {
    return {
      restrict: 'AEC',
      require: '^form',
      link: function(scope, element, attrs, form) {
        var inputs = element.find("*");
        for(var i = 0; i < inputs.length; i++) {
          (function(input){
            var attributes = input.attributes;
            if (attributes.getNamedItem('ng-model') != void 0 && attributes.getNamedItem('name') != void 0) {
              var field = form[attributes.name.value];
              if (field != void 0) {
                angular.element(input).bind('blur',function(){
                  scope.$apply(function(){
                    field.$blurred = true;
                  })
                });
                scope.$watch(function() {
                  return form.$submitted + "_" + field.$valid + "_" + field.$blurred;
                }, function() {
                  if (!field.$blurred && form.$submitted != true) return;
                  var inp = angular.element(input);
                  if (inp.hasClass('ng-invalid')) {
                    element.removeClass('has-success');
                    element.addClass('has-error');
                  } else {
                    element.removeClass('has-error').addClass('has-success');
                  }
                });
              }
            }
          })(inputs[i]);
        }
      }
    }
  }])

.directive('ensureUnique', ['$http','$rootScope', function($http,$rootScope) {
    var url=$rootScope.globalServerUrl+'/profile/';
   return {
     require: 'ngModel',
       link: function(scope, ele, attrs, c) {
        scope.$watch(attrs.ngModel, function(newValue,oldValue) {
          $http({
            url: url+attrs.route+'/',
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            data: "data="+JSON.stringify(newValue)
          }).success(function(data) {
            if(data!="false"){
              c.$setValidity('unique', false);
            } else {
              c.$setValidity('unique', true);
            }
           }).error(function(data) {
             c.$setValidity('unique', false);
           });
         });
       }
   }
 }])

.directive('match', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            scope: {
                match: '='
            },
            link: function(scope, elem, attrs, ctrl) {
                scope.$watch(function() {
                    return (ctrl.$pristine && angular.isUndefined(ctrl.$modelValue)) || scope.match === ctrl.$modelValue;
                }, function(currentValue) {
                    ctrl.$setValidity('match', currentValue);
                });
            }
        };
    })
.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
})
    .factory('ajaxServices',  function ($http,$rootScope) {
      var url = $rootScope.globalServerUrl;
        return {
          getProjects : function (id) {
            return $http({method: "GET", url: url + '/tag/getCities/' + id});
          }
        }
    })

;



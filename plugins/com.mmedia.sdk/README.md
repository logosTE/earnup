
This Cordova plugin is to add AdColony SDK to cordova project, as depency of other plugins.

# How to use? #

Write dependency entry in plugin.xml of other plugins:

```xml
	<dependency id="com.mmedia.sdk" version=">=5.4.1"/>
```

Or, add it by hand:

    cordova plugin add com.mmedia.sdk
    
# Version #

* SDK for iOS v5.4.1
* SDK for Android, v5.3.0

